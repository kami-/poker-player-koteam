var firstCase = [ // Finally the array of community cards.
    {
        "rank": "6",
        "suit": "spades"
    }, {
        "rank": "A",
        "suit": "hearts"
    }, {
        "rank": "6",
        "suit": "clubs"
    }, {
        "rank": "6",
        "suit": "hearts"
    }, {
        "rank": "6",
        "suit": "clubs"
    }

];
if(hasFourOfKind(firstCase).length != 4) {
  throw "There must be 4 cars";
}

var secondCase = [ // Finally the array of community cards.
    {
        "rank": "6",
        "suit": "spades"
    }, {
        "rank": "A",
        "suit": "hearts"
    }, {
        "rank": "6",
        "suit": "clubs"
    }, {
        "rank": "3",
        "suit": "hearts"
    }, {
        "rank": "6",
        "suit": "clubs"
    }

];
if(hasFourOfKind(secondCase).length != 0) {
  throw "There must be 0 cars";
}

var thirdCase = [ // Finally the array of community cards.
    {
        "rank": "6",
        "suit": "spades"
    }, {
        "rank": "A",
        "suit": "hearts"
    }, {
        "rank": "A",
        "suit": "clubs"
    }, {
        "rank": "A",
        "suit": "hearts"
    }

];
if(hasFourOfKind(thirdCase).length != 0) {
  throw "There must be 0 cars";
}

var fifthCase = [ // Finally the array of community cards.
    {
        "rank": "A",
        "suit": "spades"
    }, {
        "rank": "2",
        "suit": "hearts"
    }, {
        "rank": "J",
        "suit": "clubs"
    }, {
        "rank": "A",
        "suit": "hearts"
    }, {
        "rank": "A",
        "suit": "clubs"
    }, {
        "rank": "A",
        "suit": "clubs"
    }

];
if(hasFourOfKind(fifthCase).length != 4) {
  throw "There must be 4 cars";
}

var sixthCase = [ // Finally the array of community cards.
    {
        "rank": "A",
        "suit": "spades"
    }, {
        "rank": "2",
        "suit": "hearts"
    }, {
        "rank": "J",
        "suit": "clubs"
    }

];
if(hasFourOfKind(sixthCase).length != 0) {
  throw "There must be 0 cars";
}
