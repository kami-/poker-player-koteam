const request = require("request");

let GH_TOKEN = "8c6529a91bfb42c5f31be5881d1df8411c97792b";
let SET_INTERVAL_ID = null;
let CONFIG = {
  pollingInterval: 5000,
  agression: 0,
  raiseOverMinimum: 200,
  raiseProbability: 70,
  checkProbability: 30,
  betLimit: 6,
  checkLimit: 3,
  highRaiseProbability: 90
};

function updateConfig() {
    request({
      url: "https://" + GH_TOKEN + ":@api.github.com/gists/995795a18fd91527c5d542df91ffed5c",
      headers: {
        "User-Agent": "kami-"
      }
    }, (error, response, body) => {
      CONFIG = JSON.parse(JSON.parse(body).files["config.js"].content);
      console.log("Loaded config: " + JSON.stringify(CONFIG));
    });
}

if (!SET_INTERVAL_ID) {
  updateConfig();
  SET_INTERVAL_ID = setInterval(updateConfig, CONFIG.pollingInterval);
}

module.exports = CONFIG;
