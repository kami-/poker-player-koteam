
function hasStraight(cards) {
    if (cards.length < 5) { return []; }
    const reverseRankOrdering = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"].reverse();
    let reverseOrderedCards = cards.slice();
    reverseOrderedCards.sort((card1, card2) => {
        if (reverseRankOrdering.indexOf(card1.rank) < reverseRankOrdering.indexOf(card2.rank)) {
            return -1;
        } else {
            return 1;
        }
    });
    const STRAIGHTS = [
        ["A", "K", "Q", "J", "10"],
        ["K", "Q", "J", "10", "9"],
        ["Q", "J", "10", "9", "8"],
        ["J", "10", "9", "8", "7"],
        ["10", "9", "8", "7", "6"],
        ["9", "8", "7", "6", "5"],
        ["8", "7", "6", "5", "4"],
        ["7", "6", "5", "4", "3"],
        ["6", "5", "4", "3", "2"],
        ["5", "4", "3", "2", "A"]
    ];
    let checkCards = reverseOrderedCards.slice(0, 5);
    if (STRAIGHTS.some(rankOrdering => cardsMathingRankOrdering(checkCards, rankOrdering))) { return checkCards; }
    checkCards = reverseOrderedCards.slice(1, 6);
    if (STRAIGHTS.some(rankOrdering => cardsMathingRankOrdering(checkCards, rankOrdering))) { return checkCards; }
    checkCards = reverseOrderedCards.slice(2, 7);
    if (STRAIGHTS.some(rankOrdering => cardsMathingRankOrdering(checkCards, rankOrdering))) { return checkCards; }
    let aceCard = reverseOrderedCards.find(card => card.rank === "A");
    if (aceCard) {
        reverseOrderedCards.push(aceCard);
        checkCards = reverseOrderedCards.slice(3, 8);
        if (STRAIGHTS.some(rankOrdering => cardsMathingRankOrdering(checkCards, rankOrdering))) { return checkCards; }
    }
    return [];
}

function cardsMathingRankOrdering(cards, rankOrdering) {
    if (cards.length < 5) { return false; }
    let ranks = cards.map(card => card.rank);
    return ranks.every(rank => rankOrdering.includes(rank));
}

console.log(hasStraight([ { rank: "A" }, { rank: "10" } ]));
console.log(hasStraight([ { rank: "A" }, { rank: "J" }, { rank: "K" }, { rank: "10" }, { rank: "Q" } ]));
console.log(hasStraight([ { rank: "A" }, { rank: "5" }, { rank: "2" }, { rank: "3" }, { rank: "4" } ]));
console.log(hasStraight([ { rank: "A" }, { rank: "5" }, { rank: "2" }, { rank: "3" }, { rank: "4" }, { rank: "7" }, { rank: "8" } ]));
console.log(hasStraight([ { rank: "K" }, { rank: "5" }, { rank: "2" }, { rank: "3" }, { rank: "4" }, { rank: "7" }, { rank: "8" }, { rank: "6" } ]));
