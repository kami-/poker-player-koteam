const config = require('./Config'),
  pokerSolver = require('pokersolver'),
  probabilities = require('./probabilities'),
  totalPlayers = 5;

class Player {
  static get VERSION() {
    return '0.1';
  }

  static betRequest(gameState, bet) {
    console.log('Game state', gameState);
    const me = gameState.players[gameState.in_action],
      hand = me.hole_cards,
      checkAmount = gameState.current_buy_in - me.bet,
      playersLeft = gameState.players.filter(player => player.status === "active").length;
    let betAmount = 0;
    console.log('Hand', hand);
    console.log('check amount', checkAmount);
    console.log('players left', playersLeft);
    var justGoAllIn = Math.random() < 0.05;
    if (Player.awesomeHand(hand) || justGoAllIn) {
      if (justGoAllIn) {
        console.log('all in because random');
      }
      betAmount = me.stack;
    } else if (gameState.community_cards.length === 0) {
      var probability = probabilities.getProbabilitySameColor(hand[0].rank, hand[1].rank, playersLeft, hand[0].suit === hand[1].suit);
      console.log("probability of good hand", probability);
      if (me.bet === 0 && probability >= config.highRaiseProbability) {
        betAmount = checkAmount + gameState.minimum_raise + config.raiseOverMinimum;
      }else if (me.bet === 0 && probability >= config.raiseProbability  && checkAmount < (me.stack/2)) {
        betAmount = checkAmount + gameState.minimum_raise + config.raiseOverMinimum;
      } else if (probability >= config.checkProbability && checkAmount < (me.stack/3)) {
        betAmount = checkAmount;
      } else {
        betAmount = 0;
      }
    } else {
      let rank = Player.getHandRank(hand.concat(gameState.community_cards));
      if (me.bet === 0 && rank > config.betLimit) {
        betAmount = checkAmount + gameState.minimum_raise + config.raiseOverMinimum;
      } else if (rank > config.checkLimit) {
        betAmount = checkAmount;
      } else {
        betAmount = 0;
      }
    }

    console.log('betting', betAmount);
    bet(betAmount);
  }

  static showdown(gameState) {
  }

  static goodHand(hand, playersLeft, gameState) {
    let limit = 6;
    if (gameState.community_cards == 0) {
      limit = 1;
    }
    return Player.getHandRank(hand.concat(gameState.community_cards)) >= limit;
  }

  static getHandRank(cards) {
    let solverCards = cards.map(Player.toPokerSolverCard);
    return pokerSolver.Hand.solve(solverCards).rank;
  }

  static toPokerSolverCard(card) {
    const SUIT_TO_CHAR = {
        "hearts": "h",
        "spades": "s",
        "diamonds": "d",
        "clubs": "c"
    };
    return (card.rank  === "10" ? "T" : card.rank) + SUIT_TO_CHAR[card.suit];
  }

  static awesomeHand(hand) {
    return (hand[0].rank === "A" && hand[1].rank === "A") || (hand[0].rank === "K" && hand[1].rank === "K") || (hand[0].rank === "Q" && hand[1].rank === "Q");
  }
}

module.exports = Player;
